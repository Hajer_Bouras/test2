package tn.enig.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "lots")
public class Lots implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private int numLot;
	private int numDA;
	
	@Column(unique = true)
	
	@OneToMany(mappedBy = "lots", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Articles> articles;
	
	public Lots() {
		
	}

	public Lots(int numLot, int numDA) {
		super();
		this.numLot = numLot;
		this.numDA = numDA;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getNumLot() {
		return numLot;
	}

	public void setNumLot(int numLot) {
		this.numLot = numLot;
	}

	public int getNumDA() {
		return numDA;
	}

	public void setNumDA(int numDA) {
		this.numDA = numDA;
	}

	public Set<Articles> getArticles() {
		return articles;
	}

	public void setArticles(Set<Articles> articles) {
		this.articles = articles;
	}

	@Override
	public String toString() {
		return "Lots [id=" + id + ", numLot=" + numLot + ", numDA=" + numDA + ", articles="
				+ articles + "]";
	}
	
	
	
	
	

}
