package tn.enig.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "articles")
public class Articles implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private Integer id;
	private int code;
    private String designation;
    private float prixUnitaire;
    private int qteDemandé;
    private int qteLivré;
    private int qteRestant;
    private int numDA;
    private int numLot;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "lot_id", nullable = false)
    private Lots lots;
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public float getPrixUnitaire() {
		return prixUnitaire;
	}
	public void setPrixUnitaire(float prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}
	public int getQteDemandé() {
		return qteDemandé;
	}
	public void setQteDemandé(int qteDemandé) {
		this.qteDemandé = qteDemandé;
	}
	public int getQteLivré() {
		return qteLivré;
	}
	public void setQteLivré(int qteLivré) {
		this.qteLivré = qteLivré;
	}
	public int getQteRestant() {
		return qteRestant;
	}
	public void setQteRestant(int qteRestant) {
		this.qteRestant = qteRestant;
	}
	public int getNumDA() {
		return numDA;
	}
	public void setNumDA(int numDA) {
		this.numDA = numDA;
	}
	public int getNumLot() {
		return numLot;
	}
	public void setNumLot(int numLot) {
		this.numLot = numLot;
	}
	@Override
	public String toString() {
		return "Articles [id=" + id + ", code=" + code + ", designation=" + designation + ", prixUnitaire="
				+ prixUnitaire + ", qteDemandé=" + qteDemandé + ", qteLivré=" + qteLivré + ", qteRestant=" + qteRestant
				+ ", numDA=" + numDA + ", numLot=" + numLot +"]";
	}
	
	public Articles() {
		
	}
	
	public Articles(int code, String designation, float prixUnitaire, int qteDemandé, int qteLivré, int qteRestant,
			int numDA, int numLot, Lots lots) {
		super();
		this.code = code;
		this.designation = designation;
		this.prixUnitaire = prixUnitaire;
		this.qteDemandé = qteDemandé;
		this.qteLivré = qteLivré;
		this.qteRestant = qteRestant;
		this.numDA = numDA;
		this.numLot = numLot;
		this.lots = lots;
	}
	
	
    
    
    

}
