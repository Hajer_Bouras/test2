package tn.enig;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import tn.enig.model.Articles;
import tn.enig.model.Lots;
import tn.enig.repository.ArticlesRepository;
import tn.enig.repository.LotsRepository;

@SpringBootApplication
public class Test2Application  {

	public static void main(String[] args) {
		SpringApplication.run(Test2Application.class, args);
	}
	
	@Bean
    public CommandLineRunner mappingDemo(LotsRepository lotsRepository,
                                         ArticlesRepository articlesRepository) {
        return args -> {

            // create a new lot
            Lots lots = new Lots(4, 2);

            // save the lot
            lotsRepository.save(lots);

            // create and save new pages
            articlesRepository.save(new Articles(101, "Demande livré",500,10,10,0,1,4, lots));
            articlesRepository.save(new Articles(102, "Demande traiter",1000,20,18,2,2,4, lots));
            articlesRepository.save(new Articles(103, "Demande en cours de traitement",1200,30,25,5,3,4, lots));
        };
    }

}
