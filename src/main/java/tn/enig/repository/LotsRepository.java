package tn.enig.repository;

import org.springframework.data.repository.CrudRepository;

import tn.enig.model.Lots;

public interface LotsRepository extends CrudRepository<Lots, Long> {
	
	Lots findById(Integer id);

}
