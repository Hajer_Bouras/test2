package tn.enig.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import tn.enig.model.Articles;
import tn.enig.model.Lots;

public interface ArticlesRepository extends CrudRepository<Articles, Long> {
	
	List<Articles> findByLots(Lots lots, Sort sort);

}
